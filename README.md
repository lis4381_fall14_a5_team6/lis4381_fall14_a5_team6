<<<<<<< HEAD
# [HTML5 Boilerplate](http://html5boilerplate.com)

HTML5 Boilerplate is a professional front-end template for building fast,
robust, and adaptable web apps or sites.

This project is the product of many years of iterative development and combined
community knowledge. It does not impose a specific development philosophy or
framework, so you're free to architect your code in the way that you want.

* Source: [https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)
* Homepage: [http://html5boilerplate.com](http://html5boilerplate.com)
* Twitter: [@h5bp](http://twitter.com/h5bp)


## Quick start

Choose one of the following options:

1. Download the latest stable release from
   [html5boilerplate.com](http://html5boilerplate.com/) or a custom build from
   [Initializr](http://www.initializr.com).
2. Clone the git repo — `git clone
   https://github.com/h5bp/html5-boilerplate.git` - and checkout the tagged
   release you'd like to use.


## Features

* HTML5 ready. Use the new elements with confidence.
* Cross-browser compatible (Chrome, Opera, Safari, Firefox 3.6+, IE6+).
* Designed with progressive enhancement in mind.
* Includes [Normalize.css](http://necolas.github.com/normalize.css/) for CSS
  normalizations and common bug fixes.
* The latest [jQuery](http://jquery.com/) via CDN, with a local fallback.
* The latest [Modernizr](http://modernizr.com/) build for feature detection.
* IE-specific classes for easier cross-browser control.
* Placeholder CSS Media Queries.
* Useful CSS helpers.
* Default print CSS, performance optimized.
* Protection against any stray `console.log` causing JavaScript errors in
  IE6/7.
* An optimized Google Analytics snippet.
* Apache server caching, compression, and other configuration defaults for
  Grade-A performance.
* Cross-domain Ajax and Flash.
* "Delete-key friendly." Easy to strip out parts you don't need.
* Extensive inline and accompanying documentation.


## Documentation

Take a look at the [documentation table of
contents](https://github.com/h5bp/html5-boilerplate/blob/master/doc/TOC.md). This
documentation is bundled with the project, which makes it readily available for
offline reading and provides a useful starting point for any documentation
you want to write about your project.


## Contributing

Anyone and everyone is welcome to
[contribute](https://github.com/h5bp/html5-boilerplate/blob/master/CONTRIBUTING.md). Hundreds
of developers have helped make the HTML5 Boilerplate what it is today.
=======
# Team 6 #

Robert Patterson
Christopher Larocque
Alex McClurkin
Michael Ritchhart
Shaun Enriquez

### Roles/Responsibilities ###

Robert:
*Cover and Index Page
*Compile entire video

Chris:
*Individual Page
*1:15 Video

Alex:
*Individual Page
*1:15 Video

Shaun:
*Individual Page
*1:15 Video

Michael:
*Individual Page
*1:15 Video

### Who do I talk to? ###

Any of the group members


## CODIO LINKS ###
Backend - https://codio.com/rcp11h/lis4381-fall14-a5-team6/terminal/backend-1415903998999
Frontend - http://mentor-diesel.codio.io:3000/index.htm
>>>>>>> 48d1e3dc3c342260b8f56bb97765778ae564eebd
